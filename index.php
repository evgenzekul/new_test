<?php
    require_once("db/db.php");

    $first_name = $_POST['first_name'];                 // information of user
    $last_name = $_POST['last_name'];
    $phone_number = $_POST['phone_number'];
    $email = $_POST['email'];

    $street = $_POST['street_address'];
    $street_2 = $_POST['street_address_2']; 
    $state_province = $_POST['state_province'];
    $city = $_POST['city'];
    $postal = $_POST['postal_zipcode'];

    $submit = $_POST['submit'];         // variable of button 


    if ($first_name != false and $last_name != false and $phone_number != false and $email != false)
    {
        if ($street != false and $street_2 != false and $state_province != false and $city != false and $postal != false)
        {
            if (isset($submit))
            {
                mysqli_query($db_connection , "INSERT INTO `users`(`first_name`,`last_name`,`phone_number`,`e-mail`) VALUES('$first_name','$last_name','$phone_number','$email');");
                mysqli_query($db_connection , "INSERT INTO `adresses`(`street_address`,`street_address_line2`,`city`,`state_province`,`postal__zipcode`) VALUES('$street','$street_2','$city','$state_province','$postal');");
                header("Location: welcome.blade.php"); 
            }
            else
                echo "не удалось переслать данные в БД!";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <title>My Form</title>
</head>
<body>
    <form action="" class="form" method="POST">
        <div class="form__box">
            <div class="form__box-titlebox">
                <h4 class="form__box-titlebox-title">Full Name</h4>
                <p class="form__box-titlebox-redstar">*</p>
            </div>
            <div class="form__box-flex">
                <input name="first_name" class="form__box-flex-input" type="text" placeholder="First Name" required>
                <input name="last_name"class="form__box-flex-input" type="text" placeholder="Last Name" required>
            </div>
            <div class="form__box-titlebox">
                <h4 class="form__box-titlebox-title">Address</h4>
                <p class="form__box-titlebox-redstar">*</p>
            </div>
            <div class="form__box-unflex">
                <input name="street_address" class="form__box-unflex-input" type="text" placeholder="Street Address" required>  <!---->
                <input name="street_address_2"class="form__box-unflex-input" type="text" placeholder="Street Address Line 2" required>  <!---->
            </div>
            <div class="form__box-flex">
                <input name="city" class="form__box-flex-input" type="text" placeholder="City" required>
                <input name="state_province" class="form__box-flex-input" type="text" placeholder="State / Province" required>
            </div>
            <div class="form__box-unflex">
                <input name="postal_zipcode" class="form__box-unflex-input" type="text" placeholder="Postal / Zip Code" required>
                <br><br>
            </div> 
            <div class="form__box-titlebox">
                <h4 class="form__box-titlebox-title">Phone Number</h4>
                <p class="form__box-titlebox-redstar">*</p>
            </div>
            <div class="form__box-unflex">
                <input name="phone_number" class="form__box-unflex-input-small" type="text" placeholder="(000) 000-0000" required>
            </div>
            <div class="form__box-titlebox">
                <h4 class="form__box-titlebox-title">E-mail</h4>
                <p class="form__box-titlebox-redstar">*</p>
            </div>
            <div class="form__box-unflex">
                <input name="email" class="form__box-unflex-input-small" type="email" placeholder="ex: email@yahoo.com" required>
            </div>
            <hr>
        </div>
        <div class="form__submit">
            <button name="submit" class="form__submit-butt">
                Submit
            </button>
        </div>
    </form>
</body>
</html>